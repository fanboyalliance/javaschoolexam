package com.tsystems.javaschool.tasks.calculator;

public interface Separator {
    static boolean check(char c) {
        return (c == ' ' || c == '=');
    }
}
