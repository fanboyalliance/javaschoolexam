package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Count {
    public Double count(String inputString) {
        Double result = null;
        Stack<Double> doubleStack = new Stack<>();

        for (int i = 0; i < inputString.length(); i++) {
            StringBuffer sb = new StringBuffer();
            // skip all spaces and equals =
            if (Separator.check(inputString.charAt(i))) {
                continue;
            }
            // append digits to StringBuffer
            if (Character.isDigit(inputString.charAt(i))) {
                while (!Separator.check(inputString.charAt(i)) &&
                        !CheckOperations.run(inputString.charAt(i))) {
                    sb.append(inputString.charAt(i));
                    i++;
                    if (i == inputString.length()) {
                        break;
                    }
                }
                doubleStack.push(Double.parseDouble(sb.toString()));
                i--;
            // do operation
            } else if (CheckOperations.run(inputString.charAt(i))) {
                result = countResult(doubleStack.pop(), doubleStack.pop(), inputString.charAt(i));
                // division by null returns null
                if (result == null) {
                    return result;
                }
                doubleStack.push(result);
            }
        }
        return result;
    }

    private boolean checkDivisionByNull(Double num) {
        return num.intValue() == 0 && num % 1 == 0;
    }

    private Double countResult(Double a, Double b, char operation) {
        Double result = null;
        if (operation == '/' && checkDivisionByNull(a)) {
            return result;
        }
        switch (operation) {
            case '+':
                result = b + a;
                break;
            case '-':
                result = b - a;
                break;
            case '*':
                result = b * a;
                break;
            case '/':
                result = b / a;
                break;
        }
        return result;
    }
}
