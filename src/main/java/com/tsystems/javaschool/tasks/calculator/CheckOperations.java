package com.tsystems.javaschool.tasks.calculator;

public interface CheckOperations {
    static boolean run(char c) {
        return (c == '+' || c == '-' || c == '*' || c == '/');
    }
}
