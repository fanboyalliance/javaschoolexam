package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class ReversePolishNotation {
    public String reverse(String inputString) {
        StringBuffer stringBuffer = new StringBuffer();
        Stack<Character> charStack = new Stack<>();
        for (int i = 0; i < inputString.length(); i++) {
            // skip all spaces and equals =
            if (Separator.check(inputString.charAt(i))) {
                continue;
            }
            // append digits to string buffer
            if (Character.isDigit(inputString.charAt(i))) {
                while (!Separator.check(inputString.charAt(i)) &&
                        !checkOperation(inputString.charAt(i))) {
                    stringBuffer.append(inputString.charAt(i));
                    i++;
                    if (i == inputString.length()) {
                        break;
                    }
                }
                stringBuffer.append(" ");
                i--;
            }
            // add operators to stack
            if (checkOperation(inputString.charAt(i))) {
                if (inputString.charAt(i) == '(') {
                    charStack.push(inputString.charAt(i));
                } else if (inputString.charAt(i) == ')') {
                    char s = charStack.pop();
                    while (s != '(') {
                        stringBuffer.append(String.valueOf(s));
                        stringBuffer.append(" ");
                        s = charStack.pop();
                    }
                } else {
                    if (!charStack.isEmpty()) {
                        // add to StringBuffer the operator with less priority
                        if (getPriority(inputString.charAt(i)) <= getPriority(charStack.peek()) ) {
                            stringBuffer.append(charStack.pop()).toString();
                            stringBuffer.append(" ");
                        }
                    }
                    charStack.push(inputString.charAt(i));
                }
            }
        }
        // append others operators to string buffer
        while (!charStack.isEmpty()) {
            stringBuffer.append(charStack.pop());
            stringBuffer.append(" ");
        }
        return stringBuffer.toString();
    }

    private boolean checkOperation(char c) {
        return (CheckOperations.run(c) || c == '(' || c == ')');
    }

    private int getPriority(char c) {
        switch (c) {
            case '(': return 0;
            case ')': return 1;
            case '+': return 2;
            case '-': return 3;
            case '*': return 4;
            case '/': return 5;
            default: return 6;
        }
    }
}
