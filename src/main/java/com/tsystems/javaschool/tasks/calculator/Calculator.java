package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String resultStr;
        Double resultNum;
        // check string
        ValidatingString validator = new ValidatingString();
        if (!validator.run(statement)) {
            return null;
        }
        // convert to reverse polish notation
        ReversePolishNotation reversePolishNotation = new ReversePolishNotation();
        resultStr = reversePolishNotation.reverse(statement);

        // evaluate
        Count count = new Count();
        resultNum = count.count(resultStr);
        if (resultNum == null) {
            return null;
        }
        if (resultNum % 1 == 0) {
            // return integer
            return String.valueOf(resultNum.intValue());
        }
        // return double
        return resultNum.toString();
    }
}
