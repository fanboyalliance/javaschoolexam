package com.tsystems.javaschool.tasks.calculator;

public class ValidatingString {
    public boolean run(String statement) {
        // check full input string
        if (statement == null || statement.isEmpty()) {
            return false;
        }
        // check zero symbol
        if (checkNotDigits(statement.charAt(0)) || statement.charAt(0) == ',') {
            return false;
        }
        // check others
        for (int i = 1; i < statement.length(); i++) {
            if (statement.charAt(i) == ',' ||
                    (checkNotDigits(statement.charAt(i)) &&
                            statement.charAt(i) == statement.charAt(i - 1))) {
                return false;
            }
        }
        return true;
    }

    private boolean checkNotDigits(char c) {
        return (c == '+' || c == '-' || c == '*' || c == '/' || c == '(' || c == ')' || c == '.');
    }
}
