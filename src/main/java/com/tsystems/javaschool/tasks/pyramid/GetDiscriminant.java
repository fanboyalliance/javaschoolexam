package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public interface GetDiscriminant {
    static int run(List<Integer> inputNumbers) {
        // Triangle formula Tn = 1/2 * n * (n+1)
        // Discriminant = b^2 - 4a * (2c), b = 1, a = 1, c = inputNumbers.size()
        int discriminant = 1 + (4 * 1 * inputNumbers.size() * 2);
        return discriminant;
    }
}
