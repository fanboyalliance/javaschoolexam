package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Constructor {
    public int[][] build(List<Integer> inputNumbers) {
        Collections.sort(inputNumbers);
        int rows = GetRows.run(inputNumbers);
        int columns = (2 * rows - 1);

        // init matrix [rows] X [columns] and set all elements to zero
        int[][] resultArr = new int[rows][columns];
        for (int[] row : resultArr) {
            Arrays.fill(row, 0);
        }

        // middle element
        int mid = (columns / 2);
        // index of the element in the list
        int elIndex = 0;
        for (int i = 0, offset = 0, numInRows = 1; i < rows; i++, offset++, numInRows++) {
            int start = mid - offset;
            for (int j = 0; j < numInRows * 2; j += 2, elIndex++) {
                resultArr[i][start + j] = inputNumbers.get(elIndex);
            }
        }
        return resultArr;
    }
}
