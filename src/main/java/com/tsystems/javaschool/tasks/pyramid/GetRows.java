package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public interface GetRows {
    static int run(List<Integer> inputNumbers) {
        // root of the triangle formula = (-1 + discriminant) / 2
        // root == num of rows
        return ((int) Math.sqrt( (double) GetDiscriminant.run(inputNumbers) - 1) / 2);
    }
}
