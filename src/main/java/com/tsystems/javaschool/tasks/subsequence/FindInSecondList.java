package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public interface FindInSecondList {
    static boolean run(int index, List<Object> y, Object o) {
        for (int i = index; i < y.size(); i++) {
            if (y.get(i).equals(o)) {
                y.remove(i);
                return true;
            }
        }
        return false;
    }
}
