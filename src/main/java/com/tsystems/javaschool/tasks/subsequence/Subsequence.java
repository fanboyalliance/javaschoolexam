package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.isEmpty()) {
            return true;
        }
        boolean finding = false;
        int index = 0;
        for (int i = 0; i < x.size(); i++) {
            index = GetIndex.run(index, y, x.get(i));
            if (index < 0) {
                finding = false;
                break;
            }
            finding = FindInSecondList.run(index, y, x.get(i));
            if (!finding) {
                break;
            }
        }
        if (finding) {
            return true;
        }
        return false;
    }
}
