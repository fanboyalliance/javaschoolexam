package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public interface GetIndex {
    static int run(int index, List<Object> y, Object o) {
        for (int i = index; i < y.size(); i++) {
            if (y.get(i).equals(o)) {
                return i;
            }
        }
        return -1;
    }
}
